const { Schema, model } = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const validRoles = {
    values: ['USER_ROLE', 'ADMIN_ROLE'],
    message: '{VALUE} no es un rol valido'
};

const userSchema = new Schema({
    rol: {
        type: String,
        required: true,
        default: 'USER_ROLE',
        enum: validRoles,
    },
    token: {
        type: String,
        unique: true,
        trim: true
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    phone: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    password: {
        type: String,
        trim: true
    }
}, {
    timestamps: true
});

userSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });
module.exports = model('User', userSchema);
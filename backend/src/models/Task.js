const { Schema, model } = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const states = {
    values: ['TO_DO_ROLE', 'IN_PROGRESS_ROLE', 'DONE_ROLE'],
    message: '{VALUE} no es un rol valido'
};

const taskSchema = new Schema({
    idUser: {
        type: Schema.Types.ObjectId,
        required: true,
        trim: true
    },
    state: {
        type: String,
        required: true,
        default: 'TO_DO_ROLE',
        enum: states,
    },
    name: {
        type: String,
        required: true,
        trim: true,
    },
    indications: {
        type: String,
        trim: true,
    },
    description: {
        type: String,
        trim: true,
    },
    priority: {
        type: Number,
        required: true,
        trim: true,
    },
    dueDate: {
        type: String,
        required: true,
        trim: true,
    },
    comment: {
        type: String,
        trim: true
    },
    calification: {
        type: Number,
        trim: true
    },
}, {
    timestamps: true
});
taskSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });

module.exports = model('task', taskSchema);
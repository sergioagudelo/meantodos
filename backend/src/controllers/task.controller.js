const TaskCtrl = {};

const Task = require('../models/Task');
const User = require('../models/User');

TaskCtrl.getTasks = async(req, res) => {
    try {
        const _Task = await Task.find();
        if (!_Task) {
            res.status(400).json({
                ok: false,
                message: 'No se han encontrado tareas en la aplicación.',
            });
        }
        res.status(200).json({
            ok: true,
            Tasks: _Task,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
};

TaskCtrl.createTask = async(req, res) => {
    const { idUser } = req.params;
    const { name, indications, description, priority, dueDate } = req.body;


    const newTask = new Task({
        idUser: idUser,
        name,
        indications,
        description,
        priority,
        dueDate,
    });

    try {
        const _Task = await newTask.save();
        if (!_Task) {
            res.status(400).json({
                ok: true,
                message: 'No se pudo crear la tarea.',
            });
        }
        res.status(201).json({
            ok: true,
            message: 'Tarea creada.',
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
};

// 200
// 201
// 400
// 403
// 404
// 500

TaskCtrl.getTask = async(req, res) => {
    const { idTask } = req.params;

    try {
        const _Task = await Task.findById(idTask);
        if (!_Task) {
            res.status(400).json({
                ok: false,
                message: 'No se ha encontrado la tarea en la aplicación.',
            });
        }
        res.status(200).json({
            ok: true,
            Task: _Task,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }

}

TaskCtrl.getTasksByUser = async(req, res) => {
    const { idUser } = req.params;

    try {
        const _Task = await Task.find({ idUser }).sort({ dueDate: -1});
        if (!_Task) {
            res.status(400).json({
                ok: false,
                message: 'No se ha encontrado la tarea en la aplicación.',
            });
        }
        res.status(200).json({
            ok: true,
            Task: _Task,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }

}

// TaskCtrl.getServicesByClient = async(req, res) => {
//     const { idClient } = req.params;

//     try {
//         const _Service = await Service.find({ idClient: idClient });
//         if (!_Service) {
//             res.status(400).json({
//                 ok: false,
//                 message: 'No se ha encontrado el servicio en la aplicación.',
//             });
//         }
//         res.status(200).json({
//             ok: true,
//             service: _Service,
//         });
//     } catch (error) {
//         res.status(500).json({
//             ok: false,
//             message: 'Error interno del servidor, no se pudó realizar esta operación.',
//             error,
//         });
//     }
// }

TaskCtrl.deleteTask = async(req, res) => {
    const { idTask } = req.params;

    try {
        const _Task = await Task.findOneAndDelete({ _id: idTask });
        if (!_Task) {
            res.status(400).json({
                ok: false,
                message: 'No se ha encontrado la tarea en la aplicación.',
            });
        }
        res.status(200).json({
            ok: true,
            message: 'Tarea eliminada correctamente.',
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
}

TaskCtrl.updateTask = async(req, res) => {
    const { idTask } = req.params;
    const { name, indications, description, priority, dueDate, comment, calification } = req.body;

    try {
        const _Task = await Task.findOneAndUpdate({ _id: idTask }, {
            name,
            indications,
            description,
            priority,
            dueDate,
            comment,
            calification,
        }, { new: true });
        console.log(_Task)
        if (!_Task) {
            res.status(400).json({
                ok: false,
                message: 'No se pudo actualizar la tarea en la aplicación.',
            });
        }
        res.status(200).json({
            ok: true,
            message: 'Tarea actualizada',
            Task: _Task,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }

}

module.exports = TaskCtrl;
const UserCtrl = {};

const ControllersUtils = require('../utils/controllersUtils');

const User = require('../models/User');

// Status codes
// 200, 201, 400, 403, 404, 500

UserCtrl.getUsers = async(req, res) => {
    try {
        const _users = await User.find();
        if (!_users) {
            res.status(400).json({
                ok: false,
                message: 'No existen usuarios registrados en la aplicación.',
            });
        }
        //TODO: res acts as a return? breaks the method?
        res.status(200).json({
            ok: true,
            users: _users,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
};

// UserCtrl.getEmployees = async(req, res) => {
//     try {
//         const _users = await User.find({ rol: 'USER_ROLE' });
//         if (!_users) {
//             res.status(400).json({
//                 ok: false,
//                 message: 'No existen usuarios registrados en la aplicación.',
//             });
//         }
//         //TODO: res acts as a return? breaks the method?
//         res.status(200).json({
//             ok: true,
//             users: _users,
//         });
//     } catch (error) {
//         res.status(500).json({
//             ok: false,
//             error: 'Error interno del servidor, no se pudó realizar esta operación.',
//             error,
//         });
//     }
// };

UserCtrl.createUser = async(req, res) => {
    const { name, email, phone, password } = req.body;

    try {
        const userExists = await User.findOne({ 
                $or: [
                    { email: email },
                    { phone: phone }
                ]
             });

        if( !userExists ) {
            const rol = req.body.rol ? req.body.rol : 'USER_ROLE';
            const token = ControllersUtils.generateTokenForUser(name);
            const encryptedPassword = ControllersUtils.encryptPassword(password);
            const newUser = new User({
                name,
                email,
                phone,
                password: encryptedPassword,
                token,
                rol,
            });

            const _user = await newUser.save();
            _user.password = undefined;
            console.log(_user)
            if (!_user) {
                res.status(400).json({
                    ok: false,
                    message: 'No se pudo crear el usuario.',
                });
            }
            res.status(201).json({
                ok: true,
                user: _user,
            });
        } else {
            res.status(400).json({
                ok: false,
                message: 'El usuario con ese email/phone ya existe.',
            });
        }
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
};

UserCtrl.getUser = async (req, res) => {
    const { id } = req.params;

    try {
        const _user = await User.findById(id);
        if (!_user) {
            res.status(400).json({
                ok: false,
                message: 'No se ha encontrado el usuario en la aplicación',
            });
        }
        res.status(200).json({
            ok: true,
            user: _user,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
}

UserCtrl.loginUser = async (req, res) => {
    const { email, password } = req.body;

    try {
        const _user = await User.findOne({ email: email });

        if (!_user) {
            res.status(400).json({
                ok: false,
                message: 'No se ha encontrado el usuario en la aplicación',
            });
        } else {
            if( ControllersUtils.compareEncryptedPassword(password, _user.password) ) {
                _user.password = undefined;
                
                res.status(200).json({
                    ok: true,
                    user: _user,
                });
            } else {
                res.status(403).json({
                    ok: false,
                    message: "Contraseña incorrecta.",
                });
            }
        }
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
}

UserCtrl.deleteUser = async(req, res) => {
    const { id } = req.params;

    try {
        const _user = await User.findByIdAndDelete(id);
        if (!_user) {
            res.status(400).json({
                ok: false,
                message: 'No se pudo eliminar el usuario.',
            });
        }
        res.status(200).json({
            ok: true,
            message: 'User deleted',
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
}

module.exports = UserCtrl;
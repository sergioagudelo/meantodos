if (process.env.NODE_ENV !== 'production'){
    require('dotenv').config();
}

// Enviroment
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// Front End
let urlFront;
if (process.env.NODE_ENV === 'development') {
    urlFront = 'http://localhost:4200';
} else {
    urlFront = process.env.FRONT_URI;
}
process.env.FRONT_URI = urlFront;

// Database
let urlDB;
if (process.env.NODE_ENV === 'development') {
    urlDB = 'mongodb://localhost/mean-to-dos';
} else {
    urlDB = process.env.MONGODB_URI;
}
process.env.URL_DB = urlDB;

// Port
process.env.PORT = process.env.PORT || 4000;


// "postinstall": "npm run build",
// "build": "cd frontend && npm run build"
const ControllersUtils = {};

require('..//config.js');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
// Aleatory fragment used to generate the hash next to the password
const salt = 12;

ControllersUtils.generateTokenForUser = (name) => {
    let date = new Date();
    date = date.getDate() + date.getHours() + date.getMinutes() + date.getSeconds();
    const token = jwt.sign({ tokenData: name + date},
        process.env.JWT_SECRET_KEY, {
            expiresIn: process.env.JWT_EXPIRES_IN
        }); 
    
    return token;
}

ControllersUtils.encryptPassword = (password) => {
    return bcrypt.hashSync(password, salt);
}

ControllersUtils.encryptPassword = (password) => {
    return bcrypt.hashSync(password, salt);
}

ControllersUtils.compareEncryptedPassword = (providedPassword, dbPassword) => {
    return bcrypt.compareSync(providedPassword, dbPassword);
}

module.exports = ControllersUtils;
const { Router } = require('express');
const router = Router();

const TaskCtrl = require('../controllers/task.controller');

router.get('/getTasks', [], TaskCtrl.getTasks);
router.get('/getTasksByUser/:idUser', [], TaskCtrl.getTasksByUser);
// router.get('/gettasksByClient/:idClient', [], TaskCtrl.gettasksByClient);
router.post('/createTask/:idUser', [], TaskCtrl.createTask);
router.get('/getTask/:idTask', [], TaskCtrl.getTask);
router.put('/updateTask/:idTask', [], TaskCtrl.updateTask);
router.delete('/deleteTask/:idTask', [], TaskCtrl.deleteTask);

module.exports = router;
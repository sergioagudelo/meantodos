require('./config.js');
const express = require('express');
const path = require('path');
const cors = require('cors');
const morgan = require('morgan');

const app = express();

// settings
app.set('port', process.env.PORT);

// middlewares 
app.use(cors({ origin: [process.env.FRONT_URI] }));
app.use(express.json());
app.use(morgan('dev'));

// routes
app.use('/api/tasks', require('./routes/tasks'));
app.use('/api/users', require('./routes/users'));

if(process.env.NODE_ENV === 'production'){
    app.use(express.static(path.join(__dirname, 'dist/MeanToDos')));
}

app.get('*', (request, response) => {
	response.sendFile(path.join(__dirname, 'dist/MeanToDos', 'index.html'));
});

module.exports = app;
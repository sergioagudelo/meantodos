import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { ViewTasksComponent } from './components/view-tasks/view-tasks.component';
import { UserProfileComponent } from './components/profiles/user-profile/user-profile.component';
import { AdminProfileComponent } from './components/profiles/admin-profile/admin-profile.component';
import { LoginGuardGuard } from './guards/login-guard.guard';


const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'viewTasks', component: ViewTasksComponent, canActivate: [LoginGuardGuard] },
  { path: 'userProfile', component: UserProfileComponent, canActivate: [LoginGuardGuard] },
  { path: 'adminProfile', component: AdminProfileComponent, canActivate: [LoginGuardGuard] },
  { path: '**', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

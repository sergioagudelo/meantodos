import { Component, OnInit, ɵConsole } from '@angular/core';
import { UserInterface, Usuario } from 'src/app/models/usuario';
import { TasksService } from 'src/app/services/tasks.service';
import { Task } from 'src/app/models/task';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-tasks',
  templateUrl: './view-tasks.component.html',
  styleUrls: ['./view-tasks.component.css']
})
export class ViewTasksComponent implements OnInit {

  formManageTasks: FormGroup;

  user: UserInterface = new Usuario();
  
  // true if updating y false if creating
  updatingTask: boolean = false;
  idUpdatedTask: String;
  noTasks: boolean = false;

  constructor(
    public taskService: TasksService,
    private router: Router
  ) {
    this.user = JSON.parse(localStorage.getItem('USER'));
  }

  ngOnInit() {
    if ( this.user != null ) {
      this.getTasksByUsers(this.user._id);
    }
    this.initializeForms( '', '', '', null, '', '', null);
    
    if ( !localStorage.getItem('USER') ) {
      this.router.navigate(['login']);
    }
  }

  compareDates(dueDate: String){
    let tempDate = new Date(dueDate.toString());
    const tempDueDate = `${tempDate.getUTCFullYear()}-${tempDate.getUTCMonth()}-${tempDate.getUTCDay()}`;
    tempDate = new Date();
    const tempCurrentDate = `${tempDate.getFullYear()}-${tempDate.getMonth()}-${tempDate.getDate()}`
    return tempDueDate == tempCurrentDate;
  }

  getTasksByUsers(idUser: String){
    this.taskService.getTasksByUsers(idUser).subscribe(
      res => {
        this.taskService.tasks = res['Task'] as Array<Task>;
        this.noTasks = this.taskService.tasks.length <= 0 ? true : false;
        return true;
      },
      err => {
        console.log(err)
      }
    );
  }

  manageTask(form: FormGroup) {
    if ( this.updatingTask ) {
      // updating task
      this.taskService.updateTask(this.idUpdatedTask, form.value).subscribe(
        res => {
          Swal.fire(
            'Updated!',
            `task ${form.value.name} updated.`,
            'success'
          );
          form.reset();
          this.getTasksByUsers(this.user._id);
          this.noTasks = false;
        },
        err => {
          console.log(err)
        },
      );
    } else {
      // creating task
      this.taskService.createTask(this.user._id, form.value).subscribe(
        res => {
          Swal.fire(
            'Create!',
            `task ${form.value.name} created.`,
            'success'
          );
          form.reset();
          this.getTasksByUsers(this.user._id);
          this.noTasks = false;
        },
        err => {
          console.log(err)
        },
      );
    }
  }

  updatingTasks(updatingTask: boolean) {
    this.updatingTask = updatingTask;
  }

  updateTask(task: Task) {
    this.idUpdatedTask = task._id;
    this.initializeForms(task.name, task.indications, task.description, task.priority, task.dueDate, '', null);
  }

  deleteTask(task: Task) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete this task?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.taskService.deleteTask(task._id).subscribe(
          res => {
            this.getTasksByUsers(this.user._id);
            Swal.fire(
              'Deleted!',
              `task ${task.name} deleted.`,
              'success'
            );
            this.noTasks = true;
          },
          err => {
            console.log(err)
          }
        )
        this.getTasksByUsers(this.user._id);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          `task ${task.name} safe.`,
          'info'
        );
      }
    })
  }

  // Forms
  initializeForms(
    name: String,
    indications: String,
    description: String,
    priority: Number,
    dueDate: String,
    comment: String,
    calification: Number,
  ) {
    this.formManageTasks = new FormGroup({
      name: new FormControl(name, [Validators.required, Validators.maxLength(30)]),
      indications: new FormControl(indications, []),
      description: new FormControl(description, []),
      priority: new FormControl(priority, [Validators.required, Validators.min(1), Validators.max(5)]),
      dueDate: new FormControl(dueDate, [Validators.required]),
      // comment: new FormControl(comment, [Validators.required]),
      // calification: new FormControl(calification, [Validators.required]),
    });
  }
}

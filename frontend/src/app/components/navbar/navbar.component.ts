import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { UserInterface } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  user: UserInterface;
  user$: Observable<UserInterface>;

  constructor(
    private userService: UsuarioService
  ) { }

  ngOnInit() {
    this.user$ = this.userService.getUser$();
    this.user$.subscribe(user => this.user = user );

    if ( this.user == undefined && localStorage.getItem('USER')) {
      this.user = JSON.parse(localStorage.getItem('USER')) as UserInterface;
      this.userService.selectedUser(this.user);
    }
  }

  logout() {
    this.userService.logout();
  }

}
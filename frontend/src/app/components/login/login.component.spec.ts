import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let debugElement: DebugElement;
  let htmlElement: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        RouterTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement.query(By.css('.loginForm'));
    htmlElement = debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });




  
  // Login form Tests
  it('should call onLogin method', () => {
    fixture.detectChanges();
    spyOn(component, 'onLogin');
    htmlElement = fixture.debugElement.query(By.css('.loginButton')).nativeElement;
    htmlElement.click();
    expect(component.onLogin).toHaveBeenCalledTimes(0);
  });

  it('login form should be invalid', () => {
    expect(component.formLogin.valid).toBeFalsy();
  });

  it('login form should be valid', () => {
    component.formLogin.controls['email'].setValue('sergio-agudelo@hotmail.es');
    component.formLogin.controls['password'].setValue('123456');
    expect(component.formLogin.valid).toBeTruthy();
  });

  it('login form should not be valid for invalid email', () => {
    component.formLogin.controls['email'].setValue('sergio-agudelohotmail.es');
    component.formLogin.controls['password'].setValue('123456');
    expect(component.formLogin.valid).toBeFalsy();
  });

  



  // Register form tests
  it('should call onRegister method', () => {
    fixture.detectChanges();
    spyOn(component, 'onRegister');
    htmlElement = fixture.debugElement.query(By.css('.registerButton')).nativeElement;
    htmlElement.click();
    expect(component.onRegister).toHaveBeenCalledTimes(0);
  });

  it('register form should be invalid', () => {
    expect(component.formRegisterUser.valid).toBeFalsy();
  });

  it('register form should be valid', () => {
    component.formRegisterUser.controls['name'].setValue('sergio');
    component.formRegisterUser.controls['email'].setValue('sergio-agudelo@hotmail.es');
    component.formRegisterUser.controls['phone'].setValue('3182445664');
    component.formRegisterUser.controls['password'].setValue('123456');
    component.formRegisterUser.controls['passwordRepeat'].setValue('123456');
    expect(component.formRegisterUser.valid).toBeTruthy();
  });

  it('register form should not be valid for invalid email', () => {
    component.formRegisterUser.controls['name'].setValue('sergio');
    component.formRegisterUser.controls['email'].setValue('sergio-agudelo@hotmail.es');
    component.formRegisterUser.controls['phone'].setValue('318244566a');
    component.formRegisterUser.controls['password'].setValue('123456');
    component.formRegisterUser.controls['passwordRepeat'].setValue('123456');
    expect(component.formRegisterUser.valid).toBeFalsy();
  });
});

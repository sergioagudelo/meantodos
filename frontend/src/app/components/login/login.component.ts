import { Component, OnInit } from '@angular/core';

import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import Swal from 'sweetalert2'

// Services
import { UsuarioService } from 'src/app/services/usuario.service';

// Models
import { Usuario, UserInterface } from 'src/app/models/usuario';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formLogin: FormGroup;
  formRegisterUser: FormGroup;

  constructor(
    private userService: UsuarioService,
    private router: Router,
  ) {
    this.initializeForms();
  }

  ngOnInit() {
    if ( localStorage.getItem('USER') ) {
      this.router.navigate(['viewTasks']);
    }
  }

  onLogin(form: FormGroup) {
    this.userService.loginUser(form.value).subscribe(
      res => {
        this.userService.selectedUser(res['user'] as UserInterface);
        this.router.navigate(['viewTasks']);
      },
      err => {
        Swal.fire('Oops...', err.error.message, 'error')
      },
    );
  }

  onRegister(form: FormGroup) {
    if ( form.value.passwordRepeat == form.value.password ) {
      this.userService.registerUser(form.value).subscribe(
        res => {
          document.querySelector('.modal-backdrop').remove();
          this.userService.selectedUser(res['user'] as UserInterface);
          this.router.navigate(['viewTasks']);
        },
        err => {
          Swal.fire('Oops...', err.error.message, 'error')
        },
      )
    } else {
      Swal.fire('Oops...', 'Password must match.', 'error')
    }
  }

  // onRegister(form: NgForm) {
  //   this.userService.postUser
  // }

  // Forms
  initializeForms() {
    this.formLogin = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern(/[^@]+@[^\.]+\..+/)]),
      password: new FormControl('', [Validators.required, Validators.pattern(/^[A-Za-z0-9@.-<>]{5,20}/)])
    });
  
    this.formRegisterUser = new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.pattern(/[^@]+@[^\.]+\..+/)]),
      phone: new FormControl('', Validators.pattern(/^[0-9]{10}/)),
      password: new FormControl('', [Validators.required, Validators.pattern(/^[A-Za-z0-9@.-<>]{5,20}/)]),
      passwordRepeat: new FormControl('', Validators.required)
    });
  }
}

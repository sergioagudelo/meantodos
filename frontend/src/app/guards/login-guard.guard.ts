import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserInterface } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class LoginGuardGuard implements CanActivate {

  user: UserInterface;

  constructor(
    private router: Router,
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if ( !localStorage.getItem('USER') ) {
        this.router.navigate(['login']);
        return false;
      }
      return true;
  }
}

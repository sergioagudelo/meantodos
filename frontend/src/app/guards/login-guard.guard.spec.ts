import { TestBed, async, inject } from '@angular/core/testing';

import { LoginGuardGuard } from './login-guard.guard';

import { RouterTestingModule } from '@angular/router/testing';

describe('LoginGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginGuardGuard],
      imports: [
        RouterTestingModule,
      ],
    });
  });

  it('should ...', inject([LoginGuardGuard], (guard: LoginGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});

enum states { TO_DO_ROLE, IN_PROGRESS_ROLE, DONE_ROLE};

export class Task {
    _id: String;
    idUser: String;
    state: states;
    name: String;
    indications: String;
    description: String;
    priority: Number;
    dueDate: String;
    comment: String;
    calification: Number;

    constructor(
        _id = '',
        idUser = '',
        state = states.TO_DO_ROLE,
        name = '',
        indications = '',
        description = '',
        priority = 5,
        dueDate = '',
        comment = '',
        calification = null,
    ) {
        this._id = _id;
        this.idUser = idUser;
        this.state = state;
        this.name = name;
        this.indications = indications;
        this.description = description;
        this.priority = priority;
        this.dueDate = dueDate;
        this.comment = comment;
        this.calification = calification;
    }
}
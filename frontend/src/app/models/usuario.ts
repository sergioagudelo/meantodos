enum validRoles { USER_ROLE, ADMIN_ROLE }

export class Usuario {
    _id:String;
    rol: validRoles;
    token: String;
    name: String;
    email: String;
    phone: String;
    password: String;

    constructor(_id = '', rol = validRoles.USER_ROLE, token = '', name = '', email = '', phone = '', password = '') {
        this._id = _id;
        this.rol = rol;
        this.token = token;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.password = password;
    }
}

export interface UserInterface {
    _id: String;
    rol: validRoles;
    token: String;
    name: String;
    email: String;
    phone: String;
    password: String;
}
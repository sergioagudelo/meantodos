import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';

// Enviroment
import { environment } from 'src/environments/environment';

// Models
import { Usuario, UserInterface } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private userObservable$ = new Subject<UserInterface>();
  private user: UserInterface;

  readonly URL_BACK = `${environment.URL_BACK}/api/users`;

  constructor(
    public http: HttpClient
  ) {}

  registerUser(user: UserInterface) {
    return this.http.post(`${this.URL_BACK}/createUser`, user);
  }

  deleteUser(idUser: String) {
    return this.http.delete(`${this.URL_BACK}/createUser/${idUser}`);
  }

  loginUser(user: UserInterface) {
    return this.http.post(`${this.URL_BACK}/loginUser`, user);
  }

  selectedUser(user: UserInterface) {
    this.user = user;
    localStorage.setItem('USER', JSON.stringify(this.user));
    this.userObservable$.next(this.user)
  }

  getUser$(): Observable<UserInterface> {
    return this.userObservable$.asObservable();
  }

  logout() {
    localStorage.clear();
    this.user = null;
    this.userObservable$.next(null);
  }
}

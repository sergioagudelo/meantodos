import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Task } from '../models/task';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  readonly URL_BACK = `${environment.URL_BACK}/api/tasks`;
  tasks: Array<Task>;
  selectedTask: Task;

  constructor(
    public http: HttpClient
  ) { }

  createTask(idUser: String, task: Task) {
    return this.http.post(`${this.URL_BACK}/createTask/${idUser}`, task);
  }

  getTasksByUsers(idUser: String) {
    return this.http.get(`${this.URL_BACK}/getTasksByUser/${idUser}`);
  }

  updateTask(idUpdatedTask:String, task: Task) {
    return this.http.put(`${this.URL_BACK}/updateTask/${idUpdatedTask}`, task);
  }

  deleteTask(idTask: String) {
    return this.http.delete(`${this.URL_BACK}/deleteTask/${idTask}`);
  }

  chooseTask(task: Task) {
    this.selectedTask = task;
  }
}

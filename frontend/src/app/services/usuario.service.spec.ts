import { TestBed, inject } from '@angular/core/testing';

import { UsuarioService } from './usuario.service';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { request } from 'http';
import { UserInterface } from '../models/usuario';

describe('UsuarioService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule,
    ],
  }));

  it('should be created', () => {
    const service: UsuarioService = TestBed.get(UsuarioService);
    expect(service).toBeTruthy();
  });
});
